#ifndef TESTSTATE_H_INCLUDED
#define TESTSTATE_H_INCLUDED

#include <header/applicationstate.h>

class TestState : public ApplicationState {
    public:
        TestState () {
            this->state = ACTIVE;
            this->nextState = NULL;
        }

        void UpdateState(GLFWwindow* window);
        void HandleInput(GLFWwindow* window);
        ApplicationState* NextState();
    private:
};

#endif // TESTSTATE_H_INCLUDED
