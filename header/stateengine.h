#ifndef STATEENGINE_H_INCLUDED
#define STATEENGINE_H_INCLUDED

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <header/applicationstate.h>

class StateEngine {
    public:
        StateEngine* stateEngine;
        ApplicationState* currentState;

        StateEngine(ApplicationState* initialState) {
            this->currentState = initialState;
            this->stateEngine = this;
        }

        void UpdateState(GLFWwindow* window);
        void HandleInput(GLFWwindow* window);
    private:
};

#endif // STATEENGINE_H_INCLUDED
