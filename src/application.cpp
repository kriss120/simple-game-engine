/*
    Here we will set up the enviroment, create the rendering window,
    initialize the stateengine and load the initial application state.

    This also contains the main application loop and handles the application flow.
*/



#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <header/window.h>
#include <header/stateengine.h>

using namespace std;

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow *window);

const unsigned int WINDOW_HEIGHT = 600;
const unsigned int WINDOW_WIDTH = 800;

int application() {
    //Initialize GLFW
    glfwInit();

    //Create Game/Application Window
    Window* gameWindow = new Window(WINDOW_WIDTH, WINDOW_HEIGHT, "DEVEMBER!");

    //Create StateEngine and Initial State
    ApplicationState* initialState = new ApplicationState();
    StateEngine* stateEngine = new StateEngine(initialState);

    //Set openGL window context
    glfwMakeContextCurrent(gameWindow->window);
    glfwSetFramebufferSizeCallback(gameWindow->window, framebuffer_size_callback);

    //Load OpenGL with GLAD
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        cout << "Failed to initialize GLAD" << endl;
        return -1;
    }
    glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

    //Application main loop
    while(!glfwWindowShouldClose(gameWindow->window)) {
        //Handle input
        processInput(gameWindow->window);

        //Update State
        stateEngine->HandleInput(gameWindow->window);
        stateEngine->UpdateState(gameWindow->window);

    }

    glfwTerminate();
    return 0;
}

//Handles change in window size
void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

//Process Input
void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}
