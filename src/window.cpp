#include <header/window.h>

void Window::CreateWindow() {
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    GLFWwindow* newWindow = glfwCreateWindow(this->windowWidth, this->windowHeight, this->windowTitle, NULL, NULL);

    if (newWindow == NULL) {
        cout << "Failed to create GLFW window" << endl;
        glfwTerminate();
        return;
    }

    this->window = newWindow;
}
